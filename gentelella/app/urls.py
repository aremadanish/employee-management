from django.urls import path, re_path
from app import views
from django.conf import settings
from django.contrib.staticfiles.urls import static
from django.conf.urls.static import static

urlpatterns = [
    # Matches any html file - to be used for gentella
    # Avoid using your .html in your resources.
    # Or create a separate django app.
    # re_path(r'^.*\.html', views.gentella_html, name='gentella'),

    # The home page
    path('', views.index, name='index'),
    path('register/',views.register,name='register'),
    path('login/',views.login,name='login'),
    path('logout/',views.logout,name='logout'),
    path('home/',views.divert,name='divert'),
    path('addteam/',views.addteam,name='addteam'),
    path('teamsave/',views.teamsave,name='teamsave'),
    path('viewteam/',views.viewteam,name='viewteam'),
    path('persionaldetail/<str:serial>',views.persionaldetail,name='persionaldetail'),
    path('basicdetail/',views.basicdetail,name='basicdetail'),
    path('sociallinks/',views.sociallinks,name='sociallinks'),
    path('fulldetail/',views.fulldetail,name='fulldetail'),
    path('docs/<str:serial>',views.docs,name='docs'),
    path('docssave/',views.docssave,name='docssave'),
    path('joining/<str:serial>',views.joining,name='joining'),
    path('joiningsave/',views.joiningsave,name='joiningsave'),
    path('bondsave/',views.bondsave,name='bondsave'),

]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)