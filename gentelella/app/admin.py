from django.contrib import admin
from .models import Reg_User,Team_Official,Team_Personal,Team_Docs,Team_Social,Joining_Detail,Bond
# Register your models here.

admin.site.register(Reg_User)
admin.site.register(Team_Official)
admin.site.register(Team_Personal)
admin.site.register(Team_Docs)
admin.site.register(Team_Social)
admin.site.register(Joining_Detail)
admin.site.register(Bond)