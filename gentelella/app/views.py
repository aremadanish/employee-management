from django.shortcuts import render,redirect
from django.template import loader
from django.http import HttpResponse
from .models import Reg_User,Team_Official,Team_Personal,Team_Docs,Team_Social,Joining_Detail,Bond
import time
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password,check_password
import base64
from django.http import HttpResponseRedirect
from django.contrib.sessions.models import Session
from .forms import *


current_time = time.strftime('%d/%m/%Y - %H:%M')


def index(requests):
    if requests.session.has_key('is_logged'):
        return divert(requests)
        # return HttpResponseRedirect('/home')
    else:
        return render(requests,'app/login.html')

def register(requests):
    if requests.method == 'POST':
        first_name = requests.POST.get('first_name').title()
        middle_name = requests.POST.get('middle_name').title()
        last_name = requests.POST.get('last_name').title()
        email = requests.POST.get('email_address')
        username = requests.POST.get('user_name')
        user_password = requests.POST.get('user_password')
        account_type = requests.POST.get('account_type')

    if account_type == 'select':
        account_type_error = True
        params = {'account_type_error':account_type_error}
        return HttpResponseRedirect('/login.html#signup',params)
    else:
        user_data = Reg_User.objects.filter(Username=username)
        if user_data:
            username_already_taken = True
            params = {'username_already_taken':username_already_taken}
            return render(requests,'app/login.html',params)
        else:
            flag = False
            password = make_password(user_password)
            
            reg_user = Reg_User(First_Name=first_name,Middle_Name=middle_name,Last_Name=last_name,User_Email=email,Username=username,Password=password,Usertype=account_type,Reg_Date_Time=current_time)
            reg_user.save()

            params = {'success_msg':'Account Created Successfully'}
            return render(requests,'app/login.html',params)

def login(requests):
    if requests.method == 'POST':
        username = requests.POST.get('username')
        password = requests.POST.get('password')
    flag = False
    login_validate = Reg_User.objects.filter(Username=username)
    userPass = holdername = user = ''
    for e in login_validate:
        userPass = e.Password
        user = e.Usertype
        holdername = e.First_Name + ' ' + e.Last_Name
    if check_password(password,userPass) == True:
        requests.session['is_logged'] = user
        requests.session['name'] = holdername

        return divert(requests)
        
    else:
        flag = True
        params = {'flag':flag}
        return render(requests,'app/login.html',params)

def home(requests,params):
    if requests.session.has_key('is_logged'):
        return render(requests,'app/index.html',params)
    else:
        return HttpResponseRedirect('/')


def logout(request):
    del request.session['is_logged']
    return HttpResponseRedirect('/')

def divert(requests):
    try:
        if requests.session['is_logged'] == '0':
            holdername = requests.session['name']
            params = {'holdername':holdername,'menu1':'Employee Details',
                        'submenu1':'Add Team (Member)','submenu2':'View Team',
                        'link1':'addteam','link2':'viewteam'
            }
            return home(requests,params)

        elif requests.session['is_logged'] == '1':
            holdername = requests.session['name']
            params = {'holdername':holdername,'menu1':'Employee Details',
                        'submenu1':'Add Team (Member)','submenu2':'View Team',
                        'link1':'addteam','link2':'viewteam'
            }
            return home(requests,params)
    except:
        return HttpResponseRedirect('/')

def addteam(requests):
    if requests.session['is_logged'] == '0':
        holdername = requests.session['name']
        params = {'holdername':holdername,'menu1':'Employee Details',
                'submenu1':'Add Team (Member)','submenu2':'View Team',
                'link1':'addteam','link2':'viewteam',
    }
    return render(requests,'app/addteam.html',params)

def teamsave(requests):
    if requests.method == 'POST':
        team_name = requests.POST.get('name')
        team_email = requests.POST.get('email')
        team_mobile = requests.POST.get('number')
        gender = requests.POST.get('gender')
        skill = requests.POST.get('skill').title()
        typevalue = requests.POST.get('typevalue')

    name_error = Team_Official.objects.filter(Team_Name=team_name)
    email_error = Team_Official.objects.filter(Team_Email=team_email)
    mobile_error = Team_Official.objects.filter(Team_Mobile=team_mobile)
    if name_error:
        holdername = requests.session['name']
        params = {'holdername':holdername,'menu1':'Employee Details',
                    'submenu1':'Add Team (Member)','submenu2':'View Team',
                    'link1':'addteam','link2':'viewteam',
                    'name_error':True
        }
        return render(requests,'app/addteam.html',params)

    elif email_error:
        holdername = requests.session['name']
        params = {'holdername':holdername,'menu1':'Employee Details',
                    'submenu1':'Add Team (Member)','submenu2':'View Team',
                    'link1':'addteam','link2':'viewteam',
                    'email_error':True
        }
        return render(requests,'app/addteam.html',params)
    elif mobile_error:
        holdername = requests.session['name']
        params = {'holdername':holdername,'menu1':'Employee Details',
                    'submenu1':'Add Team (Member)','submenu2':'View Team',
                    'link1':'addteam','link2':'viewteam',
                    'mobile_error':True
        }
        return render(requests,'app/addteam.html',params)
    else:

        team = Team_Official(Team_Name=team_name,Team_Email=team_email,Team_Mobile=team_mobile,
            Skill=skill,Type_Value=typevalue,Team_Datetime=current_time)
        team.save()

        id_no = Team_Official.objects.filter(Team_Name=team_name)
        for i in id_no:
            team_id = i.Team_Serial

        team_personal = Team_Personal(Team_id=team_id,Team_Datetime=current_time)
        team_personal.save()

        team_docs = Team_Docs(Team_id=team_id,Team_Datetime=current_time)
        team_docs.save()

        team_social = Team_Social(Team_id=team_id,Team_Datetime=current_time)
        team_social.save()

        joining = Joining_Detail(Team_id=team_id,Team_Datetime=current_time)
        joining.save()

        bond = Bond(Team_id=team_id,Team_Datetime=current_time)
        bond.save()

        
        if requests.session['is_logged'] == '0':
            holdername = requests.session['name']
            params = {'holdername':holdername,'menu1':'Employee Details',
                    'submenu1':'Add Team (Member)','submenu2':'View Team',
                    'link1':'addteam','link2':'viewteam',
                    'success_msg':True
        }

        return render(requests,'app/addteam.html',params)


def viewteam(requests):
    team_data = Team_Official.objects.all()
    if requests.session['is_logged'] == '0':
        holdername = requests.session['name']
        params = {'holdername':holdername,'menu1':'Employee Details',
                'submenu1':'Add Team (Member)','submenu2':'View Team',
                'link1':'addteam','link2':'viewteam',
                'team_data':team_data
    }
    return render(requests,'app/viewteam.html',params)


def persionaldetail(requests,serial):
    short_data = Team_Official.objects.filter(Team_Serial=serial)
    personal_data = Team_Personal.objects.filter(Team_id=serial)
    social_data = Team_Social.objects.filter(Team_id=serial)
        
    if requests.session['is_logged'] == '0':
        holdername = requests.session['name']
        params = {'holdername':holdername,'menu1':'Employee Details',
                'submenu1':'Add Team (Member)','submenu2':'View Team',
                'link1':'addteam','link2':'viewteam',
                'serial':serial,
                'short_data':short_data,'personal_data':personal_data,'social_data':social_data
    }
    
    return render(requests,'app/updateform.html',params)

def basicdetail(requests):
    if requests.method == 'POST':
        serial = requests.POST.get('serial')
        team_name = requests.POST.get('name')
        team_email = requests.POST.get('email')
        team_mobile = requests.POST.get('number')
        gender = requests.POST.get('gender')
        skill = requests.POST.get('skill')
        typevalue = requests.POST.get('typevalue')
    short_data = Team_Official.objects.filter(Team_Serial=serial)
    personal_data = Team_Personal.objects.filter(Team_id=serial)
    social_data = Team_Social.objects.filter(Team_id=serial)
    basic = Team_Official.objects.filter(Team_Serial=serial).update(Team_Name=team_name,Team_Email=team_email,Team_Mobile=team_mobile,
        Gender=gender,Skill=skill,Type_Value=typevalue,Team_Datetime=current_time)
    if requests.session['is_logged'] == '0':
        holdername = requests.session['name']
        params = {'holdername':holdername,'menu1':'Employee Details',
                'submenu1':'Add Team (Member)','submenu2':'View Team',
                'link1':'addteam','link2':'viewteam',
                'success_msg':True,
                'serial':serial,
                'short_data':short_data,'personal_data':personal_data,'social_data':social_data
    }
    return render(requests,'app/updateform.html',params)

def sociallinks(requests):
    if requests.method == 'POST':
        serial = requests.POST.get('serial')
        instagram = requests.POST.get('instagram')
        twitter = requests.POST.get('twitter')
        whatsapp = requests.POST.get('whatsapp')
        telegram = requests.POST.get('telegram')
        hike = requests.POST.get('hike')
        linkedin = requests.POST.get('linkedin')
        skype = requests.POST.get('skype')
        slack = requests.POST.get('slack')
        facebook = requests.POST.get('facebook')
    short_data = Team_Official.objects.filter(Team_Serial=serial)
    personal_data = Team_Personal.objects.filter(Team_id=serial)
    social_data = Team_Social.objects.filter(Team_id=serial)
    social = Team_Social.objects.filter(Team_id=serial).update(Instagram=instagram,Twitter=twitter,
        Whatsapp=whatsapp,Telegram=telegram,Hike=hike,Linkedin=linkedin,Skype=skype,Slack=slack,
        Facebook=facebook,Team_Datetime=current_time)
    if requests.session['is_logged'] == '0':
        holdername = requests.session['name']
        params = {'holdername':holdername,'menu1':'Employee Details',
                'submenu1':'Add Team (Member)','submenu2':'View Team',
                'link1':'addteam','link2':'viewteam',
                'success_msg':True,
                'serial':serial,
                'short_data':short_data,'personal_data':personal_data,'social_data':social_data
    }
    return render(requests,'app/updateform.html',params)

def fulldetail(requests):
    if requests.method == 'POST':
        serial = requests.POST.get('serial')
        fathername = requests.POST.get('fathername')
        date_of_birth = requests.POST.get('dob')
        alternatecontact = requests.POST.get('alternatecontact')
        emergencycontact = requests.POST.get('emergencycontact')
        typevalue = requests.POST.get('typevalue')
        referencename1 = requests.POST.get('referencename1')
        referencenumber1 = requests.POST.get('referencenumber1')
        referencename2 = requests.POST.get('referencename2')
        referencenumber2 = requests.POST.get('referencenumber2')
        currentaddress = requests.POST.get('currentaddress')
        permanentaddress = requests.POST.get('permanentaddress')
    short_data = Team_Official.objects.filter(Team_Serial=serial)
    personal_data = Team_Personal.objects.filter(Team_id=serial)
    social_data = Team_Social.objects.filter(Team_id=serial)
    
    personal_detail = Team_Personal.objects.filter(Team_id=serial).update(Father_Name=fathername,Date_Of_Birth=date_of_birth,
        Emergency_Contact=emergencycontact,Alternate_Contact=alternatecontact,Type_Value=typevalue,
        Reference_Name1=referencename1,Reference_number1=referencenumber1,Reference_Name2=referencename2,Reference_number2=referencenumber2,
        Current_Address=currentaddress,Permanent_Address=permanentaddress,Team_Datetime=current_time)
    if requests.session['is_logged'] == '0':
        holdername = requests.session['name']
        params = {'holdername':holdername,'menu1':'Employee Details',
                'submenu1':'Add Team (Member)','submenu2':'View Team',
                'link1':'addteam','link2':'viewteam',
                'success_msg':True,
                'serial':serial,
                'short_data':short_data,'personal_data':personal_data,'social_data':social_data
    }
    return render(requests,'app/updateform.html',params)


def docs(requests,serial):
    docs = Team_Docs.objects.filter(Team_id=serial)
    for e in docs:
        panphoto = e.Team_Pan_Photo
 
    if requests.session['is_logged'] == '0':
        holdername = requests.session['name']
        params = {'holdername':holdername,'menu1':'Employee Details',
                'submenu1':'Add Team (Member)','submenu2':'View Team',
                'link1':'addteam','link2':'viewteam',
                'docs':docs,'serial':serial
    }
    return render(requests,'app/docs.html',params)

def docssave(requests):
    if requests.method == 'POST':
        serial = requests.POST.get('serial')
        docs = Team_Docs.objects.filter(Team_id=serial)
        for e in docs:
            pan = str(e.Team_Pan_Photo)
            voter = str(e.Team_Voter_Id_Photo)
            doc1 = str(e.Team_Doc1)
            doc2 = str(e.Team_Doc2)
            doc3 = str(e.Team_Doc3)
            doc4 = str(e.Team_Doc4)
        print('pan is',serial)
        team_pan = requests.POST.get('team_pan')
        try:
            team_panphoto = requests.FILES['team_panphoto']
        except:
            if pan.split('/')[1] != 'demo.jpg':
                team_panphoto = pan
            else:
                team_panphoto = 'images/demo.jpg'
        
        team_voterid = requests.POST.get('team_voterid')
        try:
            team_voteridphoto = requests.FILES['team_voteridphoto']
        except:
            if voter.split('/')[1] != 'demo.jpg':
                team_voteridphoto = voter
            else:
                team_voteridphoto = 'images/demo.jpg'

        team_account = requests.POST.get('team_account')
        team_ifsc = requests.POST.get('team_ifsc')
        team_accountname = requests.POST.get('team_accountname')
        team_cid = requests.POST.get('team_cid')

        try:
            team_doc1 = requests.FILES['team_doc1']
        except:
            if doc1.split('/')[1] != 'demo.jpg':
                team_doc1 = doc1
            else:
                team_doc1 = 'images/demo.jpg'
        try:
            team_doc2 = requests.FILES['team_doc2']
        except:
            if doc2.split('/')[1] != 'demo.jpg':
                team_doc2 = doc2
            else:
                team_doc2 = 'images/demo.jpg'
        try:
            team_doc3 = requests.FILES['team_doc3']
        except:
            if doc3.split('/')[1] != 'demo.jpg':
                team_doc3 = doc3
            else:
                team_doc3 = 'images/demo.jpg'
        try:
            team_doc4 = requests.FILES['team_doc4']
        except:
            if doc4.split('/')[1] != 'demo.jpg':
                team_doc4 = doc4
            else:
                team_doc4 = 'images/demo.jpg'
    
    document = Team_Docs.objects.get(Team_id=serial)
    document.Team_Pan = team_pan
    document.Team_Pan_Photo = team_panphoto
    document.Team_Voter_Id = team_voterid
    document.Team_Voter_Id_Photo = team_voteridphoto
    document.Team_Account = team_account
    document.Team_Ifsc = team_ifsc
    document.Team_Account_Name = team_accountname
    document.Team_cid = team_cid
    document.Team_Doc1 = team_doc1
    document.Team_Doc2 = team_doc2
    document.Team_Doc3 = team_doc3
    document.Team_Doc4 = team_doc4
    document.save()

    
    docs = Team_Docs.objects.filter(Team_id=serial)
    if requests.session['is_logged'] == '0':
        holdername = requests.session['name']
        params = {'holdername':holdername,'menu1':'Employee Details',
                'submenu1':'Add Team (Member)','submenu2':'View Team',
                'link1':'addteam','link2':'viewteam',
                'docs':docs,'serial':serial
    }
    return render(requests,'app/docs.html',params)


def joining(requests,serial):
    joining_detail = Joining_Detail.objects.filter(Team_id=serial)
    bond_detail = Bond.objects.filter(Team_id=serial)
    if requests.session['is_logged'] == '0':
        holdername = requests.session['name']
        params = {'holdername':holdername,'menu1':'Employee Details',
                'submenu1':'Add Team (Member)','submenu2':'View Team',
                'link1':'addteam','link2':'viewteam',
                'serial':serial,
                'joining_detail':joining_detail,'bond_detail':bond_detail
    }
    
    return render(requests,'app/addjoining.html',params)

def joiningsave(requests):
    if requests.method == 'POST':
        serial = requests.POST.get('serial')
        joining = requests.POST.get('joining')
        salary = requests.POST.get('salary')
        stipend = requests.POST.get('stipend')
        ctc = requests.POST.get('ctc')
        medial_facility = requests.POST.get('medial_facility')
        ltc = requests.POST.get('ltc')
        typevalue = requests.POST.get('typevalue')
        profile = requests.POST.get('profile')
        leave = requests.POST.get('leave')
        working_days = requests.POST.get('working_days')
        skill = requests.POST.get('skill')
    joining_detail = Joining_Detail.objects.filter(Team_id=serial)
    bond_detail = Bond.objects.filter(Team_id=serial)

    join = Joining_Detail.objects.filter(Team_id=serial).update(Joining=joining,Salary=salary,
        Stippend=stipend,CTC=ctc,Medical_Facility=medial_facility,LTC=ltc,Leaves=leave,Working_Days=working_days,
        Profile=profile,Skills=skill,Type_Value=typevalue,Team_Datetime=current_time)
    
    if requests.session['is_logged'] == '0':
        holdername = requests.session['name']
        params = {'holdername':holdername,'menu1':'Employee Details',
                'submenu1':'Add Team (Member)','submenu2':'View Team',
                'link1':'addteam','link2':'viewteam',
                'serial':serial,
                'joining_detail':joining_detail,'bond_detail':bond_detail
    }
    return render(requests,'app/addjoining.html',params)

def bondsave(requests):
    if requests.method == 'POST':
        serial = requests.POST.get('serial')
        bond = requests.POST.get('bond')
        amount = requests.POST.get('amount')
        offer = requests.POST.get('offer')
    joining_detail = Joining_Detail.objects.filter(Team_id=serial)
    bond_detail = Bond.objects.filter(Team_id=serial)
    
    bonddetail = Bond.objects.filter(Team_id=serial).update(Duration=bond,Amount=amount,
        Offer=offer,Team_Datetime=current_time)
    if requests.session['is_logged'] == '0':
        holdername = requests.session['name']
        params = {'holdername':holdername,'menu1':'Employee Details',
                'submenu1':'Add Team (Member)','submenu2':'View Team',
                'link1':'addteam','link2':'viewteam',
                'serial':serial,
                'joining_detail':joining_detail,'bond_detail':bond_detail
    }
    return render(requests,'app/addjoining.html',params)