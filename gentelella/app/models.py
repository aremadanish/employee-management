from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Reg_User(models.Model):
	serial = models.AutoField(primary_key=True)
	First_Name = models.CharField(max_length=30)
	Middle_Name = models.CharField(max_length=30,default='')
	Last_Name = models.CharField(max_length=30)
	User_Email = models.CharField(max_length=100)
	Username = models.CharField(max_length=50)
	Password = models.CharField(max_length=100)
	Usertype = models.CharField(max_length=1)
	Reg_Date_Time = models.CharField(max_length=30)

	def __str__(self):
		return self.First_Name


class Team_Official(models.Model):
	Team_Serial = models.AutoField(primary_key=True)
	Team_Name = models.CharField(max_length=50)
	Team_Email = models.CharField(max_length=50,default='')
	Team_Mobile = models.CharField(max_length=50,default='')
	Skill = models.CharField(max_length=50,default='')
	Gender = models.CharField(max_length=50,default='')
	Type_Value = models.CharField(max_length=50,default='')
	Team_Datetime = models.CharField(max_length=50,default='')

	def __str__(self):
		return self.Team_Name

class Team_Personal(models.Model):
	Team_Serial = models.AutoField(primary_key=True)
	Team_id = models.CharField(max_length=10,default='')
	Father_Name = models.CharField(max_length=50,default='')
	Date_Of_Birth = models.CharField(max_length=50,default='')
	Emergency_Contact = models.CharField(max_length=50,default='')
	Alternate_Contact = models.CharField(max_length=50,default='')
	Type_Value = models.CharField(max_length=50,default='')
	Reference_Name1 = models.CharField(max_length=50,default='')
	Reference_number1 = models.CharField(max_length=50,default='')
	Reference_Name2 = models.CharField(max_length=50,default='')
	Reference_number2 = models.CharField(max_length=50,default='')
	Current_Address = models.CharField(max_length=200,default='')
	Permanent_Address = models.CharField(max_length=200,default='')
	Team_Datetime = models.CharField(max_length=50,default='')

	

class Team_Docs(models.Model):
	Team_Serial = models.AutoField(primary_key=True)
	Team_id = models.CharField(max_length=10,default='')
	Team_Pan = models.CharField(max_length=50,default='')
	Team_Pan_Photo = models.ImageField(upload_to='images',default='images/demo.jpg')
	Team_Voter_Id = models.CharField(max_length=50,default='')
	Team_Voter_Id_Photo = models.ImageField(upload_to='images',default='images/demo.jpg')
	Team_Account = models.CharField(max_length=50,default='')
	Team_Ifsc = models.CharField(max_length=50,default='')
	Team_Account_Name = models.CharField(max_length=50,default='')
	Team_cid = models.CharField(max_length=50,default='')
	Team_Doc1 = models.ImageField(upload_to='images',default='images/demo.jpg')
	Team_Doc2 = models.ImageField(upload_to='images',default='images/demo.jpg')
	Team_Doc3 = models.ImageField(upload_to='images',default='images/demo.jpg')
	Team_Doc4 = models.ImageField(upload_to='images',default='images/demo.jpg')
	Team_Datetime = models.CharField(max_length=50,default='')


class Team_Social(models.Model):
	Team_Serial = models.AutoField(primary_key=True)
	Team_id = models.CharField(max_length=10,default='')
	Instagram = models.CharField(max_length=50,default='')
	Twitter = models.CharField(max_length=50,default='')
	Whatsapp = models.CharField(max_length=50,default='')
	Telegram = models.CharField(max_length=50,default='')
	Hike = models.CharField(max_length=50,default='')
	Linkedin = models.CharField(max_length=50,default='')
	Skype = models.CharField(max_length=50,default='')
	Slack = models.CharField(max_length=50,default='')
	Facebook = models.CharField(max_length=50,default='')
	Team_Datetime = models.CharField(max_length=50,default='')
	

class Joining_Detail(models.Model):
	Team_Serial = models.AutoField(primary_key=True)
	Team_id = models.CharField(max_length=10,default='')
	Name = models.CharField(max_length=50,default='')
	Joining = models.CharField(max_length=50,default='')
	Salary = models.CharField(max_length=50,default='')
	Stippend = models.CharField(max_length=50,default='')
	CTC = models.CharField(max_length=50,default='')
	Medical_Facility = models.CharField(max_length=50,default='')
	LTC = models.CharField(max_length=50,default='')
	Leaves = models.CharField(max_length=50,default='')
	Working_Days = models.CharField(max_length=50,default='')
	Profile = models.CharField(max_length=50,default='')
	Skills = models.CharField(max_length=50,default='')
	Type_Value = models.CharField(max_length=50,default='')
	Team_Datetime = models.CharField(max_length=50,default='')


class Bond(models.Model):
	Team_Serial = models.AutoField(primary_key=True)
	Team_id = models.CharField(max_length=10,default='')
	Duration = models.CharField(max_length=50,default='')
	Amount = models.CharField(max_length=50,default='')
	Offer = models.CharField(max_length=50,default='')
	Team_Datetime = models.CharField(max_length=50,default='')
