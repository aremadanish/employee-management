# Generated by Django 2.1 on 2019-12-16 13:10

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Reg_User',
            fields=[
                ('serial', models.AutoField(primary_key=True, serialize=False)),
                ('First_Name', models.CharField(max_length=30)),
                ('Middle_Name', models.CharField(max_length=30)),
                ('Last_Name', models.CharField(max_length=30)),
                ('User_Email', models.CharField(max_length=100)),
                ('Username', models.CharField(max_length=50)),
                ('Password', models.CharField(max_length=100)),
                ('Usertype', models.CharField(max_length=1)),
                ('Reg_Date_Time', models.DateField()),
            ],
        ),
    ]
