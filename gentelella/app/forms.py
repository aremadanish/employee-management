from django import forms 
from .models import *

class Team_Official_Form(forms.ModelForm):
	class Meta:
		model = Team_Official
		fields = ['Team_Name','Team_Email','Team_Mobile','Team_Datetime']

class Team_Personal_Form(forms.ModelForm):
	class Meta:
		model = Team_Personal
		fields = ['Father_Name','Date_Of_Birth','Alternate_Contact','Type_Value','Reference_Name1','Reference_number1',
		'Reference_Name2','Reference_number2','Current_Address','Permanent_Address']

class Team_Docs_Form(forms.ModelForm):
	class Meta:
		model = Team_Docs
		fields = ['Team_Pan','Team_Pan_Photo','Team_Voter_Id','Team_Voter_Id_Photo','Team_Account','Team_Ifsc',
		'Team_Account_Name','Team_cid','Team_Doc1','Team_Doc2','Team_Doc3','Team_Doc4']